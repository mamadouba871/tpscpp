#include "ObjetGraphique.h"
#include <iostream>
#include <typeinfo>

using namespace std;

int ObjetGraphique::NbObjetsGraphiques=0;

ObjetGraphique::ObjetGraphique(int couleur_,int epaisseur_,Point* PointDeBase)
{
    this->couleur_=couleur_;
    this->epaisseur_=epaisseur_;
    this->PointDeBase=PointDeBase;
    NbObjetsGraphiques++;
}

ObjetGraphique::ObjetGraphique()
{
    this->couleur_=0;
    this->epaisseur_=0;
}

ObjetGraphique::~ObjetGraphique(){
    delete this->PointDeBase;
    NbObjetsGraphiques--;
}

Point& ObjetGraphique::pointDeBase() const{
    return *this->PointDeBase;
}

int ObjetGraphique::epaisseur() const{
    return this->epaisseur_;
}

int ObjetGraphique::couleur() const{
    return this->couleur_;
}

void ObjetGraphique::modifierTrace(int nouvelleCouleur,int nouvelleEpaisseur){
    this->couleur_=nouvelleCouleur;
    this->epaisseur_=nouvelleEpaisseur;
}

void ObjetGraphique::deplacerDe(int X,int Y){
    this->PointDeBase->deplacerDe(X,Y);
}

void ObjetGraphique::deplacerVers(int versX,int versY){
    this->PointDeBase->deplacerVers(versX,versY);
}

void ObjetGraphique::afficher() const{
    cout<<"\tCouleur : "<<this->couleur()<<endl;
    cout<<"\tEpaisseur : "<<this->epaisseur()<<endl;
    cout<<"\tPoint : "<<this->PointDeBase->x()<<", "<<this->PointDeBase->y()<<endl;
}

void ObjetGraphique::effacer(){
    cout<<"Objet effac�";
}

int ObjetGraphique::GetNbObjetsGraphiques() const{
    return this->NbObjetsGraphiques;
}
