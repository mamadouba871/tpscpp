#include "Point.h"
#include <iostream>

using namespace std;

int Point::NbPoints_=0;

Point::Point()
{
    this->abscisse_=0;
    this->ordonnee_=0;
    NbPoints_++;
}

Point::~Point(){
    NbPoints_--;
}

Point::Point(double abscisse_,double ordonnee_){
    this->abscisse_=abscisse_;
    this->ordonnee_=ordonnee_;
    NbPoints_++;
}

double Point::x() const{
    return this->abscisse_;
}

double Point::y() const{
    return this->ordonnee_;
}

void Point::deplacerDe(double incX,double incY){
    this->abscisse_+=incX;
    this->ordonnee_+=incY;
}

void Point::deplacerVers(double x,double y){
    this->abscisse_=x;
    this->ordonnee_=y;
}

void Point::afficher(){
    cout<<"x : "<<this->abscisse_<<endl;
    cout<<"y : "<<this->ordonnee_<<endl;
}

int Point::NbPoints() const{
    return this->NbPoints_;
}

bool Point::isEgal(Point p) const{
    if((this->abscisse_ == p.x()) && (this->ordonnee_ == p.y())){
        return true;
    }return false;
}
