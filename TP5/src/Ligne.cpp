#include "Ligne.h"
#include <ObjetGraphique.h>
#include <Point.h>
#include <iostream>

using namespace std;

Ligne::Ligne(int couleur, int epaisseur, Point* pointDeBase, int longueur_, double angle_) :ObjetGraphique(couleur,epaisseur,pointDeBase)
{
    this->longueur_=longueur_;
    this->angle_=angle_;
}
Ligne::Ligne():ObjetGraphique()
{
    this->longueur_=0;
    this->angle_=0;
}
int Ligne::getLongueur() const{
    return this->longueur_;
}

double Ligne::getAngle() const{
    return this->angle_;
}

void Ligne::modifierLigne(int nouvelleLongueur,double nouvelAngle){
    this->longueur_=nouvelleLongueur;
    this->angle_=nouvelAngle;
}
void Ligne::afficher()const{
    cout<<"Ligne :"<<endl;
    ObjetGraphique::afficher();
    cout<<"\tLongueur : "<<this->longueur_<<endl;
    cout<<"\tAngle : "<<this->angle_<<endl;
}
