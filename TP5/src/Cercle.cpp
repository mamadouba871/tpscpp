#include "Cercle.h"
#include <ObjetGraphique.h>
#include <Point.h>
#include <iostream>

using namespace std;

Cercle::Cercle(int couleur, int epaisseur, Point* pointDeBase, int rayon_) : ObjetGraphique(couleur,epaisseur,pointDeBase)
{
    this->rayon_=rayon_;
}

int Cercle::rayon() const{
    return this->rayon_;
}

void Cercle::modifierRayon(int nouveauRayon){
    this->rayon_=nouveauRayon;
}

void Cercle::afficher() const{
    cout<<"Cercle :"<<endl;
    ObjetGraphique::afficher();
    cout<<"\tRayon : "<<this->rayon_<<endl;
}
