#include "Triangle.h"
#include <Ligne.h>
#include <iostream>
#include <math.h>

using namespace std;
#define PI 3.14159265

Triangle::Triangle(Ligne ligne1,Ligne ligne2,Ligne ligne3)
{
    if(this->isValid(ligne1, ligne2, ligne3)){
        this->ligne1=ligne1;
        this->ligne2=ligne2;
        this->ligne3=ligne3;
        cout<<"Le triangle est cr��!"<<endl;
    }else{
        cout<<"Le triangle n'est pas valide!"<<endl;
    }
}

Triangle::~Triangle()
{
    delete &this->ligne1;
    delete &this->ligne2;
    delete &this->ligne3;
}

void Triangle::afficher() const{
    cout<<"Triangle : "<<endl<<endl;
    this->ligne1.afficher();
    cout<<endl;
    this->ligne2.afficher();
    cout<<endl;
    this->ligne3.afficher();
}

bool Triangle::isValid(Ligne lg1, Ligne lg2, Ligne lg3){
    bool retour;
    Point o_lg1,e_lg1,o_lg2,e_lg2,o_lg3,e_lg3;
    double a_lg1,a_lg2, a_lg3;
    //D�finition des origines
    o_lg1 = lg1.pointDeBase();
    o_lg2 = lg2.pointDeBase();
    o_lg3 = lg3.pointDeBase();

    //D�finition des angles
    a_lg1 = lg1.getAngle();
    a_lg2 = lg2.getAngle();
    a_lg3 = lg3.getAngle();

    //Calcul des extr�mit�s
    e_lg1.deplacerDe(lg1.getLongueur()*cos(a_lg1*PI/180.0) + o_lg1.x(), lg1.getLongueur()*sin(a_lg1*PI/180.0) + o_lg1.y());
    e_lg2.deplacerDe(lg2.getLongueur()*cos(a_lg2*PI/180.0) + o_lg2.x(), lg2.getLongueur()*sin(a_lg2*PI/180.0) + o_lg2.y());
    e_lg2.deplacerDe(lg3.getLongueur()*cos(a_lg3*PI/180.0) + o_lg3.x(), lg3.getLongueur()*sin(a_lg3*PI/180.0) + o_lg3.y());
    //Si la liste des origines et extr�mit�s est compos�e de 3 doublons
    //=> Triangle valide
    //On prend le cas o� chaque origine d'une droite correspond � l'extr�mit� d'une autre
    //Sens des aiguilles d'une montre
    if((o_lg1.isEgal(e_lg3))&&(o_lg2.isEgal(e_lg1))&&(o_lg3.isEgal(e_lg2))&&(a_lg1+a_lg2+a_lg3==180)){
        retour= true;
    }
    //Sens trigonom�trique
    else if((e_lg1.isEgal(o_lg3))&&(e_lg2.isEgal(o_lg1))&&(e_lg3.isEgal(o_lg2))&&(a_lg1+a_lg2+a_lg3==180)){
        retour= true;
    }else{
        retour= false;
    }
    return retour;
}
