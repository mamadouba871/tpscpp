#include <iostream>
#include <Point.h>
#include <Cercle.h>
#include <Ligne.h>

using namespace std;

int main()
{
    Point p(1,2);

    Cercle c(15,2,&p,3);
    Ligne l(15,2,&p,3,45);

    c.afficher();
    c.deplacerDe(3,2);
    l.afficher();
    return 0;
}
