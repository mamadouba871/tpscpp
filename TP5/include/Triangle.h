#ifndef TRIANGLE_H
#define TRIANGLE_H
#include <Ligne.h>

class Triangle
{
    private:
        Ligne ligne1;
        Ligne ligne2;
        Ligne ligne3;
    public:
        Triangle(Ligne,Ligne,Ligne);
        virtual ~Triangle();
        void afficher() const;
        bool isValid(Ligne, Ligne, Ligne);
};

#endif // TRIANGLE_H
