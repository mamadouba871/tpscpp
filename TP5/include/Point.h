#ifndef POINT_H
#define POINT_H

class Point
{
    private:
        //D�finit les points en double pour la concordance
        //avec les fonctions cos et sin dans le triangle
        double abscisse_;
        double ordonnee_;
        static int NbPoints_;
    public:
        Point();
        Point(double,double);
        virtual ~Point();
        bool isEgal(Point) const;
        double x() const;
        double y() const;
        void deplacerDe(double,double);
        void deplacerVers(double,double);
        int NbPoints() const;
        void afficher();
};

#endif // POINT_H
