#ifndef LIGNE_H
#define LIGNE_H
#include <ObjetGraphique.h>
#include <Point.h>

class Ligne : public ObjetGraphique
{
    private:
        int longueur_;
        double angle_;
    public:
        Ligne(int,int,Point*,int,double);
        Ligne();
        int getLongueur() const;
        double getAngle() const;
        void modifierLigne(int,double);
        void afficher() const;
};

#endif // LIGNE_H
