#ifndef OBJETGRAPHIQUE_H
#define OBJETGRAPHIQUE_H

#include <Point.h>

class ObjetGraphique
{
    protected:
        int couleur_;
        int epaisseur_;
        Point* PointDeBase;
    private:
        static int NbObjetsGraphiques;
    public:
        ObjetGraphique(int,int,Point*);
        ObjetGraphique();
        virtual ~ObjetGraphique();
        Point& pointDeBase() const;
        int epaisseur() const;
        int couleur() const;
        void modifierTrace(int,int);
        void deplacerDe(int,int);
        void deplacerVers(int,int);
        virtual void afficher() const;
        void effacer();
        int GetNbObjetsGraphiques() const;
};

#endif // OBJETGRAPHIQUE_H
