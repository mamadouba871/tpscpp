#ifndef CERCLE_H
#define CERCLE_H
#include <ObjetGraphique.h>
#include <Point.h>

class Cercle : public ObjetGraphique
{
    private:
        int rayon_;
    public:
        Cercle(int,int,Point*,int);
        int rayon() const;
        void modifierRayon(int);
        void afficher() const;
};

#endif // CERCLE_H
