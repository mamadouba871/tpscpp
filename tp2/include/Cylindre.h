#ifndef CYLINDRE_H_INCLUDED
#define CYLINDRE_H_INCLUDED

#include <iostream>

class Cylindre {
private :
    double d; //Diam�tre du cylindre
    double h; //Hauteur du cylindre

public:
    Cylindre();
    Cylindre(double, double);
    double rayon() const;
    double surfaceBase() const;
    double volume() const;
    double perimetreBase() const;
    double surfaceLaterale() const;
    void toString() const;
    friend std::ostream & operator << (std::ostream & , const Cylindre &);
};

#endif // CYLINDRE_H_INCLUDED
