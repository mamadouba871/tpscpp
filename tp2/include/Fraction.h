#ifndef FRACTION_H
#define FRACTION_H
#include <iostream>

class Fraction
{
    public:
        int num;
        int den;

        Fraction();
        Fraction(int,int);
        void afficher() const;
        void set(int,int);
        void set(int);
        friend std::ostream & operator << (std::ostream & , const Fraction &);

        void opposer();
        Fraction opposee() const;

};

#endif // FRACTION_H
