#include "Fraction.h"
#include <stdlib.h>
#include <iostream>

Fraction:: Fraction(){
    this->num = 1;
    this->den = 1;
}
Fraction:: Fraction(int num,int den){
    this->num = num;
    this->den = den;
}
void Fraction:: afficher() const{
    //Affichage du signe
    if ((this->num/this->den) <0){
        std::cout << "Fraction : -";
    } else {
        std::cout << "Fraction : ";
    }
    //Forme r�duite
    if ((this->num)%(this->den) == 0){
        std::cout << abs(this->num/this->den);
    } else {
        std::cout << abs(this->num) << "/" << abs(this->den);
    }
}
void Fraction:: set(int num,int den){
    this->num = num;
    this->den = den;
}
void Fraction:: set(int num){
    this->num = num;
    this->den = 1;
}
std::ostream & operator << (std::ostream &os, const Fraction &F){
    //Affichage signe
    if ((F.num/F.den) <0){
        os << "Fraction : -";
    } else {
        os << "Fraction : ";
    }
    //Forme r�duite
    if ((F.num)%(F.den) == 0){
        os << abs(F.num/F.den);
    } else {
        os << abs(F.num) << "/" << abs(F.den);
    }

    return os;
}
void Fraction:: opposer(){
    this->num = -this->num;
}
Fraction Fraction:: opposee() const{
    Fraction fOpp(-this->num, this->den);
    return fOpp;
}
