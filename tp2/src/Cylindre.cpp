#include "Cylindre.h"
#include <math.h>
#include <iostream>

Cylindre:: Cylindre()
{
    this->d = 0;
    this->h = 0;
}

Cylindre:: Cylindre(double d, double h){
    this->d = d;
    this->h = h;
}
double Cylindre:: rayon() const{
    return d/2;
}
double Cylindre:: surfaceBase() const {
    return pow(rayon(), 2) * M_PI;
}
double Cylindre:: volume() const {
    return surfaceBase() * h;
}
double Cylindre:: perimetreBase() const {
    return 2 * M_PI * rayon();
}
double Cylindre:: surfaceLaterale() const {
    return perimetreBase() * h;
}
void Cylindre:: toString() const{
    std::cout << "Diametre : " << this->d << "; Hauteur : " << this->h;
}
std::ostream & operator << (std::ostream &os, const Cylindre &C){
    os << "Diametre : " << C.d << "; Hauteur : " << C.h;
    return os;
}
