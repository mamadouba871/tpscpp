#include <iostream>
#include "Cylindre.h"
#include "Fraction.h"
#include <typeinfo>

using namespace std;

int main()
{
    //Exo1
    /*double d, h;

    do {
        cout << "Saisir un diametre strictement positif : ";
        cin >> d;
    } while(d <= 0);

    cout << endl;

    do {
        cout << "Saisir une hauteur strictement positive : ";
        cin >> h;
    } while(h <= 0);

    Cylindre c(d, h);
    cout << c;*/

    //Exo2-V1
    int num, den;

    cout << "Saisir un numerateur entier pour la fraction : ";
    cin >> num;

    cout << endl;

    do {
        cout << "Saisir un denominateur entier non-nul pour la fraction : ";
        cin >> den;
    } while(den == 0);

    Fraction f1(num, den);
    cout << f1;

    //Exo2-V2
    /*Fraction f2(3, 2);
    cout << f2.opposee() << endl; //Affiche l'opposee de f2
    cout << f2 << endl; //Affiche f2 inchang�e
    f2.opposer(); //Change f2 en son opposee
    cout << f2 << endl; //Affiche f2 chang�e*/
}
