#ifndef PERSONNE_H
#define PERSONNE_H
#include <stdio.h>
#include <string>

using namespace std;
class Personne
{
    protected:
        string nom;
        string prenom;
    public:
        Personne(string, string);
        Personne();
        virtual ~Personne();
        virtual void lire(istream& is);
        virtual void ecrire(ostream& os);
        friend std::istream& operator >> (std::istream & , Personne &);
        friend std::ostream& operator << (std::ostream & , Personne &);
};

#endif // PERSONNE_H
