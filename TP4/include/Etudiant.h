#ifndef ETUDIANT_H
#define ETUDIANT_H
#include <stdio.h>
#include <string>
#include "Personne.h"
using namespace std;


class Etudiant:public Personne
{
    protected:
        string ine;
    public:
        Etudiant();
        Etudiant(string, string, string);
        virtual ~Etudiant();
        void lire(istream& is);
        void ecrire(ostream& os);
};

#endif // ETUDIANT_H
