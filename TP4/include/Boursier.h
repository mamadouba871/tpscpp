#ifndef BOURSIER_H
#define BOURSIER_H
#include <stdio.h>
#include <string>
#include "Etudiant.h"
using namespace std;


class Boursier:public Etudiant
{
    protected:
        string nD;
    public:
        Boursier();
        Boursier(string, string, string, string);
        virtual ~Boursier();
        void lire(istream& is);
        void ecrire(ostream& os);
};

#endif // BOURSIER_H
