#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include "Personne.h"
#include "Etudiant.h"
#include "Boursier.h"
using namespace std;

int main()
{
    Personne *tab[10] = {
        new Personne("Adjovi", "Amiel"),
        new Etudiant("Ba", "Mamadou", "150"),
        new Boursier("Camara", "Moussa", "599", "Dn69"),
    };
    /*tab[1]->ecrire(cout);
    cout<<endl;
    tab[1]->lire(cin);
    tab[1]->ecrire(cout);
    cout<<endl;
    delete &*tab[0];
    delete &*tab[1];
    delete &*tab[2];*/
    string rep;
    cout<<"Voulez-vous saisir une nouvelle entree o/n : ";
    int i=3;
    cin>>rep;
    while ( rep=="o" && i<10)
    {
        string type;
        cout<<"Saisir le type d'entree: ";
        cin>>type;
        transform(type.begin(), type.end(), type.begin(), ::tolower);
        if(type=="personne"){
                Personne *p = new Personne();
                //p->lire(cin);
                cin>>*p;
                tab[i]=p;
                //tab[i]->ecrire(cout);
                cout<<*tab[i];
                i++;
        }
        else if(type=="etudiant"){
                Etudiant *e = new Etudiant();
                //e->lire(cin);
                cin>>*e;
                tab[i]=e;
                //tab[i]->ecrire(cout);
                cout<<*tab[i];
                i++;
        }
        else if(type=="boursier"){
                Boursier *b = new Boursier();
                //b->lire(cin);
                cin>>*b;
                tab[i]=b;
                //tab[i]->ecrire(cout);
                cout<<*tab[i];
                i++;
        }else{
            cout<<"Les reponses doivent etre : personne, etudiant, boursier";
        }
        cout<<"Voulez-vous saisir une nouvelle entree o/n : ";
        cin>>rep;
    }
    for(int j=0; j<=i; j++){
        delete &*tab[j];
    }
}
