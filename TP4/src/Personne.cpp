#include "Personne.h"
#include <iostream>
#include <ostream>
#include <stdio.h>
#include <string>
using namespace std;

Personne::Personne()
{
    cout<<"Personne::Personne()"<<endl;
}

Personne::Personne(string n, string p)
{
    nom = n;
    prenom = p;
    cout<<"Personne::Personne("<<n<<", "<<p<<")"<<endl;
}

Personne::~Personne()
{
    cout<<"Personne::~Personne()"<<endl;
}

void Personne::lire(istream& is)
{
    cout<<"Saisir le nom"<<endl;
    is>>nom;
    cout<<"Saisir le prenom"<<endl;
    is>>prenom;
}

void Personne::ecrire(ostream& os)
{
    os<<"Personne( "<<nom<<", "<<prenom<<" )";
}
std::istream& operator >> (std::istream &is, Personne &p){
    p.lire(is);
    return is;
}
std::ostream& operator << (std::ostream &os, Personne &p){
    p.ecrire(os);
    return os;
}
