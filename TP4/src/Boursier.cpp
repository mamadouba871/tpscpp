#include "Boursier.h"
#include <iostream>
#include <ostream>
#include <stdio.h>
#include <string>
using namespace std;

Boursier::Boursier()
{
    cout<<"Boursier::Boursier()"<<endl;
}

Boursier::~Boursier()
{
    cout<<"Boursier::~Boursier()"<<endl;
}

Boursier::Boursier(string n, string p, string i, string d):Etudiant(n, p, i)
{
    nD = d;
    cout<<"Boursier::Boursier("<<n<<", "<<p<<", "<<i<<", "<<d<<")"<<endl;
}

void Boursier::lire(istream& is)
{
    Etudiant::lire(is);
    cout<<"Saisir numero dossier"<<endl;
    is>>nD;
}

void Boursier::ecrire(ostream& os)
{
    os<<"Boursier( ";
    Etudiant::ecrire(os);
    os<<", "<<nD<<" )";
}
