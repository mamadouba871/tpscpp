#include "Etudiant.h"
#include <iostream>
#include <ostream>
#include <stdio.h>
#include <string>
using namespace std;

Etudiant::Etudiant()
{
    cout<<"Etudiant::Etudiant()"<<endl;
}

Etudiant::~Etudiant()
{
    cout<<"Etudiant::~Etudiant()"<<endl;
}

Etudiant::Etudiant(string n, string p, string i):Personne(n, p)
{
    ine = i;
    cout<<"Etudiant::Etudiant("<<n<<", "<<p<<", "<<i<<")"<<endl;
}

void Etudiant::lire(istream& is)
{
    Personne::lire(is);
    cout<<"Saisir numero INE"<<endl;
    is>>ine;
}

void Etudiant::ecrire(ostream& os)
{
    os<<"Etudiant( ";
    Personne::ecrire(os);
    os<<", "<<ine<<" )";
}
