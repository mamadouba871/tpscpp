#include <iostream>
#include <stdlib.h>

using namespace std;

void exo1()
{
    int i, n;

    cout << "Tables de multiplication" << endl<<endl;
    cout << "Entrez le nombre : ";
    cin >> n;

    while(n<0 || n>20)
    {
      cout<<"Entrer un nombre compris entre 0 et 20 exclus : ";
      cin >> n;
    }

    cout<<"Table du nombre "<<n<<" :"<<endl;
    for(i=1; i<=n; i++)
    {
        cout<<n<<"x"<<i<<"="<<n*i<<endl;
    }

}

inline int minimum(int a, int b)
{
    int m=a;
    if(a>b)
        m=b;
    return m;
}

inline int maximum(int a, int b)
{
    int m=a;
    if(a<b)
        m=b;
    return m;
}
void exo2()
{
    int a, b;
    cout << "Entrez le premier nombre : ";
    cin >> a;

    cout << "Entrez le deuxi�me nombre : ";
    cin >> b;

    cout<<"Le min est : "<<minimum(a,b)<<endl;
    cout<<"Le max est : "<<maximum(a,b)<<endl;
}

void permuteByPointeur(int *a, int *b)
{
   *a=*a+*b;
   *b=*a-*b;
   *a=*a-*b;

}

void permuteByRef(int &a, int &b)
{
    a=a+b;
    b=a-b;
    a=a-b;
}

void exo3()
{
    int a, b;
    cout << "Entrez le premier nombre : ";
    cin >> a;

    cout << "Entrez le deuxi�me nombre : ";
    cin >> b;

    //permuteByPointeur(&a, &b);
    permuteByRef(a, b);
    cout<<"La nouvelle valeur du premier nombre est : "<<a<<endl;
    cout<<"La nouvelle valeur du deuxi�me nombre est : "<<b<<endl;
}

int ABS(int a)
{
    return abs(a);
}

double ABS(double a)
{
    double v=a;
    if(a<0) v=-a;
    return v;
}

void exo4()
{
    //int a;
    //double a;
    float a;// Ca marche toujours
    //cout << "Entrez un entier : ";
    cout << "Entrez un d�cimal : ";
    cin >> a;
    cout<<"La valeur absolue du nombre est : "<<ABS(a);

}

int main()
{
    exo4();
    return 0;
}

