#include "Eleve.h"
#include "Notes.h"
#include <iostream>
#include <vector>
#include <iterator>
#include <string>

Eleve::Eleve(unsigned short t):notes(t){}

Eleve::Eleve(std::string n, std::string p , std::vector<double> v, unsigned short t): notes(v, t)
{
    nom=n;
    prenom=p;
}

Eleve::~Eleve()
{
    notes.~Notes();
}

void Eleve::nouvelleIdentite(){
    std::string n, p;
    std::cout<<"Saisir le nom : ";
    std::cin>>n;
    std::cout<<std::endl;
    std::cout<<"Saisir le prenom : ";
    std::cin>>p;
    std::cout<<std::endl;
    nom=n;
    prenom=p;
}
void Eleve::ajouterNote(double n){
    notes.ajout(n);
}
void Eleve::supprimerNote(){
    notes.enlever();
}
void Eleve::visualiser(){
    std::cout<<"Nom : "<<nom<<std::endl;
    std::cout<<"Prenom : "<<prenom<<std::endl;
    std::cout<<"Mes notes sont : ";
    notes.lireNotes();
}

std::istream& operator >> (std::istream &is, Eleve &e){

    std::string rep;
    std::vector<double> v;
    double val;
    unsigned short i;
    unsigned short t;
    unsigned short nbre;

    //On d�finit une nouvelle identit�
    e.nouvelleIdentite();
    //On demande le nombre de note maximale qu'aura l'�l�ve
    std::cout<<"Saisir le nombre de notes maximal : ";
    is>>t;
    std::cout<<std::endl;
    //On d�finit l'instance Notes de notre objet Eleve
    e.notes = Notes(t);
    std::cout<<"Saisir le nombre de notes que vous souhaitez initialement enregistrer : ";
    is>>nbre;
    std::cout<<std::endl;
    //On s'assure que e nombre de notes � saisir n'est pas sup�rieur � la taille de notre vecteur
    while (nbre>t){
        std::cout<<"Saisir un nombre de notes inf�rieur � "<<t<<" : ";
        is>>nbre;
    }

    for (i=0; i<nbre; i++){
        std::cout<<"Saisir la note "<<i+1<<" : ";
        is>>val;
        std::cout<<std::endl;
        //On rajoute une nouvelle note � l'�l�ve
        e.ajouterNote(val);

        std::cout<<"Voulez-vous saisir la note suivante ? (o/n) : ";
        is>>rep;
        std::cout<<std::endl;
        while (rep!="o" && rep!="O" && rep!="n" && rep!="N"){
            std::cout<<"R�pondre o/n : ";
            is>>rep;
        }
        //Si l'utilisateur ne veut plus saisir de note, on arr�te
        if (rep=="n" || rep=="N") {
            break;
        }
    }
    return is;
}
