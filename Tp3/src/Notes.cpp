#include "Notes.h"
#include <math.h>
#include <iostream>
#include <vector>
#include <iterator>

Notes::Notes(std::vector<double> v, unsigned short t)
{
    notes = v;
    taille = t;
    nombreNotes = notes.size();
}

Notes::Notes(unsigned short t)
{
    notes = {};
    taille = t;
    nombreNotes = notes.size();
}

Notes::~Notes()
{
    delete &notes;
}
void Notes::ajout(double note){
    if(notes.size()<taille){
        notes.push_back(note);
        nombreNotes = notes.size();
    }else{
        std::cout<<"La note ne peut pas �tre ajout�e. La taille est : "<<notes.size()<<std::endl;
    }

}
void Notes::enlever(){
    if(!notes.empty()){
        notes.pop_back();
        nombreNotes = notes.size();
    }else{
        std::cout<<"In n'y a aucune note"<<std::endl;
    }
}
double Notes::moyenne(){
    double sum=0;
    double avg=0;
    if(notes.empty()){
        return avg;
    }
    std::vector<double>::iterator itv;
    for(itv=notes.begin(); itv<notes.end(); itv++){
        sum += *itv;
    }
    avg=sum/notes.size();
    return avg;
}
double Notes::plusPetite(){
    double min=20;
    std::vector<double>::iterator itv;
    for(itv=notes.begin(); itv<notes.end(); itv++){
        if(min>*itv){
            min=*itv;
        }
    }
    return min;
}
double Notes::plusGrande(){
    double max=0;
    std::vector<double>::iterator itv;
    for(itv=notes.begin(); itv<notes.end(); itv++){
        if(max<*itv){
            max=*itv;
        }
    }
    return max;
}
double Notes::lireNote(unsigned short i) const{
    if(i<notes.size()){
        return notes.at(i);
    }else{
        std::cout << "Le nombre n'existe pas. On retourne 0 par d�faut"<< std::endl;
        return 0;
    }
    /*try{
        return notes.at(i);
    }catch (const std::out_of_range & ex)
	{
		std::cout << "out_of_range Exception Caught :: " << ex.what() << std::endl;
	}*/
}
void Notes::lireNotes() const{
    unsigned short i;
    for(i= 0; i<notes.size(); i++){
        std::cout << Notes::lireNote(i)<< std::endl;
    }
}
unsigned short Notes::nombre() const{
    return nombreNotes;
}
