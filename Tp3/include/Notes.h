#ifndef NOTES_H
#define NOTES_H
#include <vector>

class Notes
{

    private:
        std::vector<double> notes;
        unsigned short taille;
        unsigned short nombreNotes;
    public:
        Notes(std::vector<double>, unsigned short);
        Notes(unsigned short);
        virtual ~Notes();
        void ajout(double);
        void enlever();
        double moyenne();
        double plusPetite();
        double plusGrande();
        double lireNote(unsigned short) const;
        void lireNotes() const;
        unsigned short nombre() const;
};

#endif // NOTES_H
