#ifndef ELEVE_H
#define ELEVE_H
#include "Notes.h"
#include <vector>
#include <string>

class Eleve
{
    private:
        std::string nom;
        std::string prenom;
        Notes notes;
    public:
        Eleve(unsigned short);
        Eleve(std::string, std::string, std::vector<double>, unsigned short);
        virtual ~Eleve();
        void nouvelleIdentite();
        void ajouterNote(double);
        void supprimerNote();
        void visualiser();
        friend std::istream& operator >> (std::istream & , Eleve &);
};

#endif // ELEVE_H
