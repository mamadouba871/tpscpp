#include <iostream>
#include <vector>
#include "Notes.h"
#include "Eleve.h"

using namespace std;

int main()
{
    vector<double> v;
    v.assign(1, 12);
    Notes n(v, 3);
    n.ajout(14);
    n.ajout(13);
    //n.ajout(10);
    //n.enlever();
    //cout << n.lireNote(0) << endl;
    //cout << n.lireNote(2) << endl;
    //cout << n.plusPetite() << endl;
    Eleve e(4);
    cin >> e;
    e.visualiser();
    return 0;
}
